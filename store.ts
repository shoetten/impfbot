import { SimpleFsStorageProvider } from "matrix-bot-sdk"
type Impfzentrum = {
  name: string,
  available: number,
}
type Subscriptions = {
  [roomId:string]: string[],
}
type StoreData = {
  subscribedRoomIds: Subscriptions,
  availableImpfzentren: Impfzentrum[],
}

export default class Store {
  private _storage: SimpleFsStorageProvider

  constructor(storageProvider: SimpleFsStorageProvider) {
    this._storage = storageProvider
  }

  private get data(): StoreData {
    // @ts-ignore
    return this._storage.readValue('impfbot') || {}
  }

  private set data(data: StoreData) {
    // @ts-ignore
    this._storage.storeValue('impfbot', data)
  }

  public getSubscriptions(): Subscriptions {
    return this.data.subscribedRoomIds || {}
  }

  public addSubscription(roomId: string) {
    this.data = {
      ...this.data,
      subscribedRoomIds: {
        ...this.data.subscribedRoomIds,
        [roomId]: [],
      },
    }
  }

  public removeSubscription(roomId: string) {
    const subscribedRoomIds = this.data.subscribedRoomIds
    delete subscribedRoomIds[roomId]
    this.data = {
      ...this.data,
      subscribedRoomIds,
    }
  }

  public saveFilter(roomId: string, filter: string[]) {
    this.data = {
      ...this.data,
      subscribedRoomIds: {
        ...this.data.subscribedRoomIds,
        [roomId]: filter,
      },
    }
  }

  public getAvailableImpfzentren(): Impfzentrum[] {
    return this.data.availableImpfzentren || []
  }

  public saveAvailableImpfzentren(availableImpfzentren: Impfzentrum[]) {
    this.data = {
      ...this.data,
      availableImpfzentren,
    }
  }
}
