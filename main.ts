import {
  MatrixEvent,
  SimpleFsStorageProvider,
  AutojoinRoomsMixin,
} from 'matrix-bot-sdk'
import fetch from 'node-fetch'
import { isEqual } from 'lodash'

import {
  HOMESERVER_URL,
  ACCESS_TOKEN,
  API_URL,
  CHECK_INTERVAL,
  AVAILABILITY_THRESHOLD,
  UPDATE_THRESHOLD,
} from './config'
import Store from './store'
import ImpfbotClient from './impfbot_client'

const storageProvider = new SimpleFsStorageProvider('data/storage.json')
const store = new Store(storageProvider)

const client = new ImpfbotClient(HOMESERVER_URL, ACCESS_TOKEN, storageProvider)
AutojoinRoomsMixin.setupOnClient(client)

client.on('room.join', async (roomId) => {
  const welcomeMessage = `<strong>Hey, ich bin der Impfbot für Sachsen!</strong>
  <br>
  Du kannst mich mit folgenden Befehlen steuern:
  <ul>
    <li><code>!impfzentren</code>: Alle Impfzentren in Sachsen anzeigen</li>
    <li><code>!filter IMPFZENTRUM1, IMPFZENTRUM2</code> (komma getrennt): Zeige nur Updates aus bestimmten Impfzentren</li>
    <li><code>!binschongeimpft</code>: Impfbot abbestellen</li>
  </ul>`

  await client.sendMessage(roomId, welcomeMessage)
  await addSubscription(roomId)
})

client.on('room.message', handleCommand)

async function handleCommand(roomId: string, event: MatrixEvent<any>) {
  if (!event['content']) return

  // Don't handle non-text events
  if (event['content']['msgtype'] !== 'm.text') return

  // filter out events sent by the bot itself
  if (event['sender'] === await client.getUserId()) return

  // Make sure that the event looks like a command we're expecting
  const body = event['content']['body']
  if (!body || (body[0] !== '!')) return

  const [command, arg] = body.split(/\s(.+)/)

  switch(command) {
    case '!impfmich':
      await addSubscription(roomId)
      break

    case '!impfzentren':
    case '!list':
      await listImpfzentren(roomId)
      break

    case '!filter':
      await saveImpfzentrenFilter(roomId, arg)
      break

    case '!binschongeimpft':
      await removeSubscription(roomId)
      break

    case '!gimme':
      client.sendMessage(roomId, 'https://youtu.be/v509w85NGOc')
      break
  }
}

async function addSubscription(roomId: string) {
  store.addSubscription(roomId)

  const availableImpfzentren = await getAvailableImpfzentren()

  await client.sendMessage(roomId, `Ich melde mich, sobald es freie Impftermine gibt! Aktuell sieht's so aus:<br>${updateMessage(availableImpfzentren)}`)
}

async function removeSubscription(roomId: string) {
  store.removeSubscription(roomId)

  await client.sendMessage(roomId, 'Herzlichen Glückwunsch zum Impftermin! 🎉 Von mir kriegste keine Nachrichten mehr.')
}

async function listImpfzentren(roomId: string) {
  const impfzentren = (await getImpfzentren())
    .map((impfzentrum) => impfzentrum.name)
    .join(', ')
  const message = `Es gibt folgende Impfzentren:
  <br>
  ${impfzentren}
  <br>
  Schreibe mir <code>!filter Impfzentrum 1, Impfzentrum 2</code> (komma getrennt) um nur Updates aus bestimmten Impfzentren zu bekommen.`

  await client.sendMessage(roomId, message)
}

async function saveImpfzentrenFilter(roomId: string, arg: string) {
  if (arg === undefined) {
    const message = `Bitte gib die Impfzentren an, von denen du Updates bekommen möchtest.
    <br>
    Beispielsweise <code>!filter Leipzig, Borna, Grimma</code> (komma getrennt) um nur Updates aus Leipzig, Borna und Grimma zu bekommen.
    <br>
    Mit <code>!impfzentren</code> bekommst du alle verfügbaren Impfzentren angezeigt.`

    await client.sendMessage(roomId, message)
    return
  }

  const filter = arg.split(',').map((token) => token.trim())
  store.saveFilter(roomId, filter)

  await client.sendMessage(roomId, `Okay. Ab jetzt bekommst du nur noch Updates aus ${filter.join(', ')}.`)
}

async function getImpfzentren() {
  let data
  try {
    data = (await (await fetch(API_URL)).json()).response.data
  } catch (error) {
    console.error(error)
    return []
  }

  return Object.values(data).map((impfzentrum) => ({
    // @ts-ignore
    name: impfzentrum.name,
    // @ts-ignore
    available: impfzentrum.counteritems[0].val,
  }))
}

async function getAvailableImpfzentren() {
  return (await getImpfzentren()).filter((impfzentrum) => impfzentrum.available >= AVAILABILITY_THRESHOLD)
}

function updateMessage(availableImpfzentren): string {
  if (availableImpfzentren.length === 0) return 'Aktuell gibt es leider keine freien Impftermine.'
  return availableImpfzentren
    .map((impfzentrum) => `${impfzentrum.name}: ${impfzentrum.available} verfügbare Termine`)
    .join('<br>')
}

function filterImpfzentren(impfzentren, filter: string[]) {
  if (filter.length === 0)
    return impfzentren

  return impfzentren.filter((impfzentrum) => (
    filter.some((entry) => impfzentrum.name.match(new RegExp(entry, 'i')))
  ))
}

async function notify() {
  const availableImpfzentren = await getAvailableImpfzentren()
  const storedImpfzentren = store.getAvailableImpfzentren()
  if (isEqual(availableImpfzentren, storedImpfzentren)) return
  const currentSum = availableImpfzentren.reduce((sum, impfzentrum) => sum + impfzentrum.available, 0)
  const storedSum = storedImpfzentren.reduce((sum, impfzentrum) => sum + impfzentrum.available, 0)
  if (Math.abs(currentSum - storedSum) < UPDATE_THRESHOLD) return
  store.saveAvailableImpfzentren(availableImpfzentren)

  if (availableImpfzentren.length === 0) return

  const subscriptions = store.getSubscriptions()

  for (const [roomId, filter] of Object.entries(subscriptions)) {
    const myAvailableImpfzentren = filterImpfzentren(availableImpfzentren, filter)
    const myStoredImpfzentren = filterImpfzentren(storedImpfzentren, filter)
    if (isEqual(myAvailableImpfzentren, myStoredImpfzentren)) continue

    await client.sendMessage(roomId, `Es gibt Updates bei den Impfterminen! 🎉<br>${updateMessage(myAvailableImpfzentren)}`)
  }
}

client.start().then(() => {
  setInterval(notify, CHECK_INTERVAL)
  console.log('Impfbot started!')
})
