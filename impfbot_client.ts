import { MatrixClient } from 'matrix-bot-sdk'
import * as sanitizeHtml from "sanitize-html"

export default class ImpfbotClient extends MatrixClient {
  async sendMessage(roomId: string, message: string): Promise<string> {
    return await super.sendMessage(roomId, {
      msgtype: 'm.text',
      format: 'org.matrix.custom.html',
      body: sanitizeHtml(message.replace(/<br>/g, '\n')),
      formatted_body: message,
    })
  }
}
