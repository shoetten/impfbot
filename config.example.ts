// where you would point a client to talk to a homeserver
export const HOMESERVER_URL = ''

// see https://t2bot.io/docs/access_tokens
export const ACCESS_TOKEN = ''

// where is the data
export const API_URL = 'https://www.startupuniverse.ch/api/1.1/de/counters/getAll/_iz_sachsen'

// check impftermine every two minutes and a bit
export const CHECK_INTERVAL = 120300

// how many available impftermine counts as available?
export const AVAILABILITY_THRESHOLD = 10

// by how many impftermine does the counter have to change, to send an update message
export const UPDATE_THRESHOLD = 30
