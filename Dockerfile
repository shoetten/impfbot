FROM node:14
ENV NODE_ENV=production

RUN mkdir /opt/app && chown node:node /opt/app
RUN mkdir /opt/app/data && chown -R node:node /opt/app/data
WORKDIR /opt/app

COPY . .

USER node

RUN yarn install --non-interactive
RUN yarn build

CMD [ "yarn", "start" ]
