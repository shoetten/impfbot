# Impfbot Sachsen

Matrix bot (e.g. for [Element](https://element.io/)) to notify you, when an impftermin is available in sachsen.

Chat with me! Invite `@impfbot_sachsen:livingutopia.org` to an unencrypted room.

## Commands

You can command me by sending me messages!

- `!impfzentren`: list all impfzentren in sachsen
- `!filter IMPFZENTRUM1, IMPFZENTRUM2` (comma separated list of impfzentren): show only results for specified impfzentren
- `!binschongeimpft`: unsubscribe
- `!impfmich`: resubscribe

## Patents vs. the people

If you have the chance to get vaccinated, you're priviledged. So on a political note:

**[Gebt die Patente frei!](https://www.patents-kill.org/deutsch/)**  #patentskill

## ?!? Was ist dieses Element und wie bekomme ich jetzt endlich einen Impftermin?

Als erstes musst du die Element App installieren. Die gibbes entweder im [F-Droid](https://f-droid.org/packages/im.vector.app/), im [Google Play Store](https://play.google.com/store/apps/details?id=im.vector.app) oder im [iOS App Store](https://apps.apple.com/de/app/element-messenger/id1083446067#?platform=iphone). Danach guckste dir dieses Video hier an:

![Impfbot Tutorial](docs/impfbot_tutorial.webm)

PS: Neben [chat.livingutopia.org](https://chat.livingutopia.org/), gibt es noch ganz viele [andere tolle Homeserver](https://chat.privacytools.io/), die ihr [mit Element benutzen könnt](https://www.hello-matrix.net/public_servers.php).

PPS: Das Video ist für Android gemacht, bei iOS sieht es ein bissches anders aus. Wenn du mir ein iPhone schenkst, mach ich auch ein Video mit iOS.

PPPS: Element ist nicht nur für Impftermine ein guter Messenger. Ich würde sogar sagen, Element ist in 90% der Fälle [der beste Messenger](https://shoetten.github.io/secure-messaging/#/10), den es gibt. Also vergiss What's App, Telegram und wie sie alle heißen und installier dir Element noch heute! :)

PPPPS: [!gimme, gimme, gimmeeee....](https://youtu.be/v509w85NGOc)

## Run your own impfbot

Clone and configure

```sh
git clone https://gitlab.com/shoetten/impfbot.git
cd impfbot
cp config.example.ts config.ts && vim config.ts
```

Run

```sh
sudo docker volume create impfbot-data
sudo docker build --tag impfbot .
sudo docker run --init -d --restart unless-stopped --read-only --tmpfs /tmp -v impfbot-data:/opt/app/data --name impfbot impfbot:latest
```

## Contributing

Feel free to send merge requests. The are just two rules :)

- 1. Your code has to be at least as hacky as mine
- 2. you don't spend much time on it
